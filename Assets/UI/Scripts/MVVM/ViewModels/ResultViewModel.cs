﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Scripts.MVVM.Interfaces;
using UniRx;

namespace UI.Scripts.MVVM.ViewModels
{
    public class ResultViewModel : IResultViewModel
    {
        public UniRx.IObservable<int> Score
        {
            get
            {
                return _score.AsObservable();
            }
        }

        private ReactiveProperty<int> _score = new ReactiveProperty<int>();

        public ResultViewModel(int score)
        {
            _score.Value = score;
        }
    }
}
