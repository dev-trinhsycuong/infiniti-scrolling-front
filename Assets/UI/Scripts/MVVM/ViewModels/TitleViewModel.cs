﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Scripts.MVVM.Interfaces;
using UnityEngine.SceneManagement;
using UI.Scripts.Route;

namespace UI.Scripts.MVVM.ViewModels
{
    /// <summary>
    /// Data class view
    /// </summary>
    public class TitleViewModel : ITitleViewModel
    {
        /// <summary>
        /// Raises the click start button event.
        /// </summary>
        public void OnClickStartButton()
        {
            PageRouter.Instance.LoadMainScene();
        }

        /// <summary>
        /// Raises the click quit button event.
        /// </summary>
        public void OnClickQuitButton()
        {
            Application.Quit();
        }
    }
}
