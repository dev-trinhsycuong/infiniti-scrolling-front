﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Scripts.MVVM.Interfaces;
using UnityEngine.SceneManagement;
using UI.Scripts.Route;

namespace UI.Scripts.MVVM.ViewModels
{
    /// <summary>
    /// Data class view
    /// </summary>
    public class PauseViewModel : IPauseViewModel
    {
        /// <summary>
        /// Raises the click start button event.
        /// </summary>
        public void OnClickCloseButton()
        {
            Time.timeScale = 1f;
            PageRouter.Instance.ClearCurrentPage();
        }

        /// <summary>
        /// Raises the click quit button event.
        /// </summary>
        public void OnClickQuitButton()
        {
            Application.Quit();
        }
    }
}
