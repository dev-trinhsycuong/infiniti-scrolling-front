﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace UI.Scripts.MVVM.Interfaces
{
    /// <summary>
    /// Data class view
    /// </summary>
    public interface IResultViewModel
    {
        IObservable<int> Score{ get; }
    }
}
