﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Scripts.MVVM.Interfaces
{
    /// <summary>
    /// Data class view
    /// </summary>
    public interface IPauseViewModel
    {
        /// <summary>
        /// Raises the click start button event.
        /// </summary>
        void OnClickCloseButton();

        /// <summary>
        /// Raises the click quit button event.
        /// </summary>
        void OnClickQuitButton();
    }
}
