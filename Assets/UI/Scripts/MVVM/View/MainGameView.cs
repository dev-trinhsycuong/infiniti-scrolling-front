﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Game.Scripts;
using UniRx;
using UI.Scripts.Route;
using UI.Scripts.PageTransitions.MainGame;

namespace UI.Scripts.MVVM.Views
{
    public class MainGameView : MonoBehaviour 
    {
        [SerializeField] private Text _scoreText;

        [SerializeField] private Button _pauseBtn;

    	// Use this for initialization
    	void Start () 
        {
            GameStatictis.Score.AsObservable().SubscribeToText(_scoreText).AddTo(this);

            _pauseBtn.OnClickAsObservable().Subscribe(_ =>
                {
                    PageRouter.Instance.DoTransition<PausePageTransition>();
                }).AddTo(this);
    	}
    }
}
