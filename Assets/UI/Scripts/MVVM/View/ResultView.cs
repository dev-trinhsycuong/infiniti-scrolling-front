﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Scripts.MVVM.ViewModels;
using UnityEngine.UI;
using UniRx;
using UI.Scripts.MVVM.Interfaces;

namespace UI.Scripts.MVVM.Views
{
    public class ResultView : MonoBehaviour
    {
        private const string FINAL_SCORE_TEXT = "Your score : {0}";

        [SerializeField] private Text _scoreText;

        public void Bind(IResultViewModel viewModel)
        {
            viewModel.Score.Subscribe(score =>
                {
                    _scoreText.text = string.Format(FINAL_SCORE_TEXT,score);
                }).AddTo(this);
        }
    }
}
