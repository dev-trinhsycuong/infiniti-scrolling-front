﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Scripts.MVVM.Interfaces;
using UnityEngine.UI;
using UniRx;

namespace UI.Scripts.MVVM.Views
{
    public class TitleView : MonoBehaviour 
    {
        [SerializeField] private Button _startBtn,_quitBtn;

        private ITitleViewModel _viewModel;

    	// Use this for initialization
    	void Start ()
        {
            _startBtn.OnClickAsObservable().Subscribe(_ =>
                {
                    _viewModel.OnClickStartButton();
                });

            _quitBtn.OnClickAsObservable().Subscribe(_ =>
                {
                    _viewModel.OnClickQuitButton();
                });
    	}

        public void Bind(ITitleViewModel viewModel)
        {
            _viewModel = viewModel;
        }
    }
}
