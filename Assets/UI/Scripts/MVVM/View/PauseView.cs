﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Scripts.MVVM.ViewModels;
using UnityEngine.UI;
using UniRx;
using UI.Scripts.MVVM.Interfaces;

namespace UI.Scripts.MVVM.Views
{
    public class PauseView : MonoBehaviour
    {
        [SerializeField] private Button _closeBtn,_quitBtn;

        void Start()
        {
            Time.timeScale = 0f;
        }

        public void Bind(IPauseViewModel viewModel)
        {
            _closeBtn.OnClickAsObservable().Subscribe(_ =>
                {
                    viewModel.OnClickCloseButton();
                }).AddTo(this);

            _quitBtn.OnClickAsObservable().Subscribe(_ =>
                {
                    viewModel.OnClickQuitButton();
                }).AddTo(this);
        }
    }
}
