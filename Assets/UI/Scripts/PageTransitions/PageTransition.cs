﻿using UnityEngine;
using UniRx;
using System.Collections;

namespace UI.Scripts.PageTransitions
{
    /// <summary>
    /// Base class of page transition
    /// </summary>
    public abstract class PageTransition : ScriptableObject
    {
        [SerializeField] protected GameObject _pagePrefab;

        protected GameObject _pageInstance;
        public GameObject PageInstance {
            get { return _pageInstance; }
        }

        protected PageTransition()
        {
        }
        
        /// <summary>
        /// Maybe we can fetch data from back-end,so fetch data here
        /// </summary>
        public abstract IEnumerator LoadAsync();

        private void Awake()
        {
        }
    
        
        /// <summary>
        /// Instantiate page on screen
        /// </summary>
        public virtual void InstantiatePage(Transform parent)
        {  
            _pageInstance = (GameObject) Instantiate(_pagePrefab, parent);
        }

        /// <summary>
        /// After fetch data,we bind data to ViewModel
        /// </summary>
        public abstract void BindLoadedModels();

        /// <summary>
        /// Destroy page instance
        /// </summary>
        public void DestroyPageInstance()
        {
            Destroy(_pageInstance);
        }

    }

}

