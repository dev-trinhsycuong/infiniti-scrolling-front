﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Scripts.MVVM.ViewModels;
using UI.Scripts.MVVM.Views;

namespace UI.Scripts.PageTransitions.Tittle
{
    public class TittleTransition : PageTransition 
    {
        public override IEnumerator LoadAsync()
        {
            // Nothing data to fetch from back-end so early return here
            yield return null;
        }

        /// <summary>
        /// After fetch data,we bind data to ViewModel,nothing data to fetch at this page,so ViewModel is empty
        /// </summary>
        public override void BindLoadedModels()
        {
            var viewModel = new TitleViewModel();
            _pageInstance.GetComponent<TitleView>().Bind(viewModel);
        }
    }
}
