﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Scripts.MVVM.ViewModels;
using UI.Scripts.MVVM.Views;
using Game.Scripts;

namespace UI.Scripts.PageTransitions.MainGame
{
    public class PausePageTransition : PageTransition
    {
        public override IEnumerator LoadAsync()
        {
            // Nothing to fetch from back-end so just early return
            yield return null;
        }
        public override void BindLoadedModels()
        {
            var viewModel = new PauseViewModel();
            _pageInstance.GetComponent<PauseView>().Bind(viewModel);
        }
    }
}
