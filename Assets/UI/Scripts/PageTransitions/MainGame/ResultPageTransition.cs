﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Scripts.MVVM.ViewModels;
using UI.Scripts.MVVM.Views;
using Game.Scripts;

namespace UI.Scripts.PageTransitions.MainGame
{
    public class ResultPageTransition : PageTransition
    {
        public override IEnumerator LoadAsync()
        {
            // Nothing to fetch from back-end so just early return
            yield return null;
        }
        public override void BindLoadedModels()
        {
            var viewModel = new ResultViewModel(GameStatictis.Score.Value);
            _pageInstance.GetComponent<ResultView>().Bind(viewModel);
        }
    }
}
