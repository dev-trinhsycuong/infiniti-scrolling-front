﻿using UnityEngine;

namespace UI.Scripts.Common
{
    [DisallowMultipleComponent]
    public class NodeFactory : MonoBehaviour
    {
        /// <summary>
        /// Prefab which will be instanciated by node
        /// </summary>
        [SerializeField]
        private GameObject Prefab;

        /// <summary>
        /// Allow create on Awake or not
        /// </summary>
        [SerializeField]
        private bool CreateOnAwake;

        /// <summary>
        /// Instance of prefab
        /// </summary>
        private GameObject _instance;
        public GameObject Instance
        {
            get { return GetOrCreate(); }
        }

        /// <summary>
        /// This node has instance or not
        /// </summary>
        public bool HasInstance
        {
            get { return _instance != null; }
        }

        /// <summary>
        /// Destroy instance of an node
        /// </summary>
        public void Destroy()
        {
            Destroy(_instance);
            _instance = null;
        }

        /// <summary>
        /// if node was instanciated,just get,if not we create node
        /// </summary>
        /// <returns></returns>
        GameObject GetOrCreate()
        {
            return _instance ?? (_instance = CreateInstance());
        }

        /// <summary>
        /// Create instance of node
        /// </summary>
        /// <returns></returns>
        GameObject CreateInstance()
        {
            var result = Instantiate(Prefab, transform, false);
            return result;
        }

        void Awake()
        {
            if (CreateOnAwake)
            {
                GetOrCreate();
            }
        }

        /// <summary>
        /// Set active node
        /// </summary>
        /// <param name="isActive"></param>
        public void SetActive(bool isActive)
        {
            gameObject.SetActive(isActive);
        }
    }
}
