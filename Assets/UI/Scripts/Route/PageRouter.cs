﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UI.Scripts.PageTransitions;
using UnityEngine.SceneManagement;

namespace UI.Scripts.Route
{
    /// <summary>
    /// 
    /// </summary>
    public class PageRouter : MonoBehaviour
    {
        /// <summary>
        /// Page router instance
        /// </summary>
        public static PageRouter Instance { get; private set; }

        [SerializeField]
        private List<PageTransition> _transitions;
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        public List<PageTransition> PageTransitions { get { return _transitions; } }
#endif

        [SerializeField]
        private PageTransition _initializedTransitions;

        /// <summary>
        /// Current page transition
        /// </summary>
        private static PageTransition _currentPageTransition;

        public PageTransition CurrentPageTransition { get { return _currentPageTransition; } }

        /// <summary>
        /// Which page transtition will transit to
        /// </summary>
        private static PageTransition _nextPageTransition;

        public PageTransition NextPageTransition { get { return _nextPageTransition; } }

        /// <summary>
        /// Coroutine to transition to page
        /// </summary>
        /// <param name="pageTransition"></param>
        /// <param name="isForce"></param>
        public IEnumerator Transition(PageTransition pageTransition, bool isForce = false)
        {
            _nextPageTransition = pageTransition;
            if (!isForce && _currentPageTransition != null &&
                _currentPageTransition.name.Equals(_nextPageTransition.name))
            {
                yield break; //if open the same pageTransition return early                
            }

            yield return _nextPageTransition.LoadAsync();

            if (_currentPageTransition != null)
            {
                _currentPageTransition.DestroyPageInstance();
            }

            _nextPageTransition.InstantiatePage(transform);
            _currentPageTransition = _nextPageTransition;
            yield return null;
            _nextPageTransition.BindLoadedModels();
        }

        /// <summary>
        /// Load main scene
        /// </summary>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public void LoadMainScene()
        {
            StartCoroutine(DoLoadMainScene());
        }

        /// <summary>
        /// Transition to specific page
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void DoTransition<T>(bool isForce = false) where T : PageTransition
        {
            var pageTransition = GetTransition<T>();
            StartCoroutine(Transition(pageTransition, isForce));
        }

        public T GetTransition<T>() where T : PageTransition
        {
            return (T) _transitions.First(transition => transition.GetType() == typeof(T));
        }

        /// <summary>
        /// Get Current Page Transition Instantiate
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public T GetCurrentTransition<T>() where T : PageTransition
        {
            return (T) _currentPageTransition;
        }

        /// <summary>
        /// Get next Page Transition Instantiate
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public T GetNextTransition<T>() where T : PageTransition
        {
            return (T) _nextPageTransition;
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(Instance.gameObject);
                return;
            }

            DestroyImmediate(gameObject);
        }

        // Use this for initialization
        private void Start()
        {
            if (_initializedTransitions != null)
            {
                StartCoroutine(Transition(_initializedTransitions, true));
            }
        }

        private void OnDestroy()
        {
            if (_currentPageTransition)
            {
                _currentPageTransition.DestroyPageInstance();
                _currentPageTransition = null;
            }
        }

        /// <summary>
        /// ClearCurrentPage
        /// </summary>
        public void ClearCurrentPage()
        {
            if (_currentPageTransition)
            {
                _currentPageTransition.DestroyPageInstance();
                _currentPageTransition = null;
            }
        }

        private IEnumerator DoLoadMainScene()
        {
            yield return SceneManager.LoadSceneAsync("MainGame");

            ClearCurrentPage();
        }
    }
}