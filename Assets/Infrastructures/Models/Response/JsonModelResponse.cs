﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Infrastructures.Models.Response
{
    public class JsonModelResponse
    {
        public int responseCode;

        public string message;
    }
}
