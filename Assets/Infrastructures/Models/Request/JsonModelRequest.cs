﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Infrastructures.Models.Request
{
    public class JsonModelRequest
    {
        private Dictionary<string,object> _data;

        public JsonModelRequest(Dictionary<string,object> models)
        {
            _data = models;
        }

        public string ToString()
        {
            var data = "{";

            var keys = _data.Keys.ToList();

            keys.ForEach(x =>
                {
                    data += x + ":";
                    object value;
                    _data.TryGetValue(x,out value);

                    data += value + ",";
                });

            data += "}";

            return data;
        }
    }
}
