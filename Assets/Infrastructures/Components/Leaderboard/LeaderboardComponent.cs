using UniRx;
using Infrastructures.Domains.Leaderboard;
using Infrastructures.Repository.Leaderboard;

namespace Infrastructures.Components.Leaderboard
{
    /// <summary>
    /// LeaderboardComponent
    /// </summary>
    public class LeaderboardComponent
    {
        /// <summary>
        /// Instance of LeaderboardComponent
        /// </summary>
        private static LeaderboardComponent _instance;

        /// <summary>
        /// Instance of LeaderboardComponent
        /// </summary>
        public static LeaderboardComponent Instance
        {
            get
            {
                if (_instance != null) return _instance;
                _instance = new LeaderboardComponent(LeaderboardRepository.GetInstance());

                return _instance;
            }
        }

        /// <summary>
        /// Instance of ILeaderboardRepository
        /// </summary>
        private readonly ILeaderboardRepository _repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository">ILeaderboardRepository</param>
        private LeaderboardComponent(ILeaderboardRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns>LeaderboardInformationParameter information Observable</returns>
        public IObservable<LeaderboardInformationParameter> Save(string userName,int score)
        {
            return _repository.Save(userName,score);
        }
    }
}