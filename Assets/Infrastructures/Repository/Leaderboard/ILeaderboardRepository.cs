using UniRx;
using Infrastructures.Domains.Leaderboard;

namespace Infrastructures.Repository.Leaderboard
{
    /// <summary>
    /// ILeaderboardRepository
    /// </summary>
    public interface ILeaderboardRepository
    {
        /// <summary>
        /// Save
        /// </summary>
        /// <returns>LeaderboardInformationParameter observable</returns>
        IObservable<LeaderboardInformationParameter> Save(string userName,int score);
    }
}