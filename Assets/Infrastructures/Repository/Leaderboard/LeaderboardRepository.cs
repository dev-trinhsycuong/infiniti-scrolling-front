using UniRx;
using Infrastructures.Domains.Leaderboard;
using Infrastructures.Communication;
using Infrastructures.Factories.Leaderboard;
using System.Collections.Generic;
using Infrastructures.Models;
using Infrastructures.Models.Request;
using Infrastructures.Models.Response;
using UnityEngine;
using Infrastructures.Models.Response.Leaderboard;

namespace Infrastructures.Repository.Leaderboard
{
    /// <summary>
    /// LeaderboardRepository
    /// </summary>
    public class LeaderboardRepository : ILeaderboardRepository
    {
        /// <summary>
        /// Instance of LeaderboardRepository
        /// </summary>
        private static LeaderboardRepository _instance;

        /// <summary>
        /// Get instance of LeaderboardRepository
        /// </summary>
        /// <returns>LeaderboardRepository</returns>
        public static LeaderboardRepository GetInstance()
        {
            if (_instance != null) return _instance;

            _instance = new LeaderboardRepository(ApiClient.GetInstance());
            return _instance;
        }

        /// <summary>
        /// IApiClient
        /// </summary>
        private IApiClient _apiClient;

        /// <summary>
        /// save leaderboard endpoint
        /// </summary>
        private const string SAVE_END_POINT = "/leaderboard";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="apiClient">API client.</param>
        public LeaderboardRepository(IApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns>LeaderboardInformationParameter observable</returns>
        public IObservable<LeaderboardInformationParameter> Save(string userName,int score)
        {
            var models = new Dictionary<string,object>()
            {
                { "userName",userName },
                { "score",score }
            };

            return _apiClient.Post(SAVE_END_POINT, new JsonModelRequest(models)).Select(json =>
            {
                var dummyResponse = JsonUtility.FromJson<LeaderboardJsonModelResponse>(json);

                return LeaderboardFactory.Make(dummyResponse);
            });
        }
    }
}