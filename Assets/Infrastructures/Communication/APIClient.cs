﻿using UniRx;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Infrastructures.Models;
using Infrastructures.Models.Request;

namespace Infrastructures.Communication
{
    public enum RequestType
    {
        GET,
        POST,
    }

    public class ApiClient : IApiClient
    {
        /// <summary>
        /// Base URI
        /// </summary>
        private static string _baseUri { get { return "https://localhost:8080"; } }

        /// <summary>
        /// IApiClient
        /// </summary>
        private static IApiClient _instance;

        /// <summary>
        /// GetInstance
        /// </summary>
        /// <returns></returns>
        public static IApiClient GetInstance()
        {
            if (_instance != null)
            {
                return _instance;
            }

            var apiClient = new ApiClient();

            _instance = apiClient;
            return _instance;
        }               

        public IObservable<string> Get(string path, JsonModelRequest models)
        {
            return Observable.FromCoroutineValue<string>(() => Send(_baseUri + path,RequestType.GET,models));
        }

        public IObservable<string> Post(string path, JsonModelRequest models)
        {
            return Observable.FromCoroutineValue<string>(() => Send(_baseUri + path,RequestType.POST,models));
        }

        IEnumerator Send(string path, RequestType type, JsonModelRequest models)
        {
            // Create a Web Form
            WWWForm form = new WWWForm();
            var data = models.ToString();
            form.AddField("data", data);

            var w = type == RequestType.GET ? UnityWebRequest.Get(path) : UnityWebRequest.Post(path, form);

            // Temporarily comment this line and return an dummy
            // yield return w.SendWebRequest();

            yield return DummyResponse();
        }

        string DummyResponse()
        {
            return "{\"responseCode\":404,\"message\":\"Username not found (user has not registered with the leaderboard service)\"}";
        }
    }
}
