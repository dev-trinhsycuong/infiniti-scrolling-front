﻿using UniRx;
using System.Collections.Generic;
using Infrastructures.Models;
using Infrastructures.Models.Request;

namespace Infrastructures.Communication
{
    public interface IApiClient
    {
        /// <summary>
        /// GET request
        /// </summary>
        /// <param name="path">URI</param>
        IObservable<string> Get(string path, JsonModelRequest models);

        /// <summary>
        /// POST request
        /// </summary>
        /// <param name="path"></param>
        /// <param name="models"></param>
        IObservable<string> Post(string path, JsonModelRequest models);
    }
}