using Infrastructures.Domains.Leaderboard;
using Infrastructures.Models.Response;

namespace Infrastructures.Factories.Leaderboard
{
    /// <summary>
    /// LeaderboardFactory
    /// </summary>
    public static class LeaderboardFactory
    {        
        /// <summary>
        /// Make LeaderboardInformationParameter from json
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns>LeaderboardInformationParameter</returns>
        public static LeaderboardInformationParameter Make(JsonModelResponse response)
        {
            return new LeaderboardInformationParameter(response.responseCode, response.message);
        }
    }
}