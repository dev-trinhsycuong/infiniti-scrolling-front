﻿using System.Collections.Generic;

namespace Infrastructures.Domains.Leaderboard
{
    /// <summary>
    /// LeaderboardInformationParameter
    /// </summary>
    public class LeaderboardInformationParameter
    {
        public int _responseCode;

        public string _message;

        /// <summary>
        /// Constructor
        /// </summary>
        public LeaderboardInformationParameter(int respondeCode,string message)
        {
            _responseCode = respondeCode;
            _message = message;
        }
    }
}