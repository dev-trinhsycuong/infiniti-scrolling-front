﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Game.Scripts
{
    public class GameStatictis
    {
        public static ReactiveProperty<int> Score = new ReactiveProperty<int>(0);

        public static void Reset()
        {
            Score.Value = 0;
        }
    }
}
