﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Game.Scripts.Pooling
{
    public class ObjectPooling : MonoBehaviour 
    {
        public List<PoolObject> ActiveObject
        {
            get
            {
                return _pooledObject.Where(x => x.gameObject.activeSelf).ToList();
            }
        }

        [SerializeField] private PoolObject _gameObject;

        [SerializeField] private int _capacity = 10;

        [SerializeField] private int _extentCapacity = 5;

        private List<PoolObject> _pooledObject = new List<PoolObject>();

        void Start()
        {
            for (int i = 1; i <= _capacity; i++)
            {
                Pool();
            }
        }

        PoolObject Pool()
        {
            var pooledObject = Instantiate(_gameObject);
            pooledObject.transform.parent = transform;
            pooledObject.gameObject.SetActive(false);

            _pooledObject.Add(pooledObject);

            return pooledObject;
        }

        public PoolObject Pop()
        {
            var noMoreObject = _pooledObject.All(x => x.gameObject.activeSelf);

            // If all actived,we extent capacity
            if (noMoreObject)
            {
                for (int i = 1; i <= _extentCapacity; i++)
                {
                    Pool();
                }
            }

            return _pooledObject.FirstOrDefault(x => !x.gameObject.activeSelf);
        }

        public void Push(PoolObject pooledObject)
        {
            pooledObject.gameObject.SetActive(false);
        }
    }
}
