﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Scripts.Pooling;
using UniRx;

namespace Game.Scripts
{
    public class EnemyController : PoolObject 
    {
        public bool IsStop = false;

        private bool _isPassed = false;

        void Start()
        {
            Observable.EveryUpdate().Subscribe(_ =>
                {
                    if(IsStop) return;

                    Move();
                });
        }

        void Move()
        {
            var newPosition = transform.position;
            newPosition.x += GameDefines.ENEMY_MOVE_SPEED * Time.deltaTime;

            bool isEnded = newPosition.x >= GameDefines.ENEMY_END_POSITION.x;

            transform.position = isEnded ? GameDefines.ENEMY_INITIAL_POSITION : newPosition;

            if (isEnded)
            {
                _isPassed = false;
                GameController.Instance.EnemyObjectPooling.Push(this);
                return;
            }

            if (!_isPassed && newPosition.x >= GameController.Instance.CharacterController.transform.position.x)
            {
                _isPassed = true;
                GameController.Instance.PassedObstacle();
            }
        }
    }
}
