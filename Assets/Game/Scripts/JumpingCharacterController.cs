﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UniRx;

namespace Game.Scripts
{
    public enum JumpCharacterState
    {
        RUNNING,
        JUMPING,
        DEAD
    }

    /// <summary>
    /// Class to control jumping character
    /// Because we had an scrolling background so we don't move x axis,we just move y axis when character jump
    /// </summary>
    public class JumpingCharacterController : MonoBehaviour 
    {
        /// <summary>
        /// Character state
        /// </summary>
        private ReactiveProperty<JumpCharacterState> _state =  new ReactiveProperty<JumpCharacterState>(JumpCharacterState.RUNNING);

        /// <summary>
        /// Initialize
        /// </summary>
        void Start()
        {
            // Publish changes
            _state.AsObservable().Subscribe(state =>
                {
                    Action(state);
                });

            // Replace for update method
            Observable.EveryUpdate().Subscribe(_ =>
                {
                    if (Input.GetMouseButtonDown(0) && _state.Value == JumpCharacterState.RUNNING)
                    {
                        _state.Value = JumpCharacterState.JUMPING;
                    }
                });
        }

        void Action(JumpCharacterState state)
        {
            switch (state)
            {
                case JumpCharacterState.JUMPING:
                    {
                        Jump();
                    }
                    break;
                case JumpCharacterState.DEAD:
                    {
                        GameController.Instance.EndGame();
                    }
                    break;
            }
        }

        void Jump()
        {
            transform.DOJump(transform.position,1f,1,0.5f).OnComplete(()=>
                {
                    _state.Value = JumpCharacterState.RUNNING;
                });
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            _state.Value = JumpCharacterState.DEAD;
        }
    }
}
