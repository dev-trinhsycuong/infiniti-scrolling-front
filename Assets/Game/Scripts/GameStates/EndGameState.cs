﻿using UnityEngine;
using UI.Scripts.Route;
using UI.Scripts.PageTransitions.MainGame;
using Infrastructures.Components.Leaderboard;
using UniRx;

namespace Game.Scripts.GameStates
{
    public class EndGameState : BaseGameState
    {
        public void Process()
        {
            // Show result UI
            PageRouter.Instance.DoTransition<ResultPageTransition>();

            // Submit leaderboard
            LeaderboardComponent.Instance.Save(GameDefines.USER_NAME, GameStatictis.Score.Value).Subscribe(result =>
                {
                    Debug.Log(result._responseCode + ":" + result._message);
                });

            // Stop game
            Time.timeScale = 0f;
        }
    }
}
