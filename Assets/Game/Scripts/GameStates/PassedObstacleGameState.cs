﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Scripts.GameStates
{
    public class PassedObstacleGameState : BaseGameState 
    {
        public void Process()
        {
            // Receive the score
            GameStatictis.Score.Value += GameDefines.SCORE_FOR_EACH_PASSING;
            
            // Continue to play
            GameController.Instance.Continue();
        }
    }
}
