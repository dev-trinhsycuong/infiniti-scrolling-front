﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Scripts.GameStates
{
    public interface BaseGameState 
    {
        void Process();
    }
}
