﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Scripts.Pooling;
using UniRx;
using Game.Scripts.GameStates;
using System;
using UI.Scripts.Common;

namespace Game.Scripts
{
    /// <summary>
    /// Control game flow
    /// </summary>
    public class GameController : MonoBehaviour 
    {
        public static GameController Instance
        { 
            get
            { 
                return _instance;
            } 
        }

        public ObjectPooling EnemyObjectPooling
        { 
            get
            { 
                return _enemyObjectPooling;
            } 
        }

        public JumpingCharacterController CharacterController
        {
            get
            {
                return _characterController;
            }
        }

        private static GameController _instance;

        [SerializeField] private NodeFactory _characterNode;

        [SerializeField] private ObjectPooling _enemyObjectPooling;

        private Dictionary<Type,BaseGameState> _stateMaps = new Dictionary<Type, BaseGameState>()
        {
            {typeof(PlayingGameState),new PlayingGameState()},
            {typeof(PassedObstacleGameState),new PassedObstacleGameState()},
            {typeof(EndGameState),new EndGameState()},
        };

        private ReactiveProperty<BaseGameState> _state = new ReactiveProperty<BaseGameState>(new PlayingGameState());

        private JumpingCharacterController _characterController;

        void Awake()
        {
            _instance = this;

            _characterController = _characterNode.Instance.GetComponent<JumpingCharacterController>();
        }

        void Start()
        {
            _state.AsObservable().Subscribe(_ =>
                {
                    _state.Value.Process();
                });
        }

        void Update()
        {
            WaveController.Instance.Update();
        }

        public void Continue()
        {
            _state.Value = _stateMaps[typeof(PlayingGameState)];
        }

        public void PassedObstacle()
        {
            _state.Value = _stateMaps[typeof(PassedObstacleGameState)];
        }

        public void EndGame()
        {
            _state.Value = _stateMaps[typeof(EndGameState)];
        }
    }
}
