﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Scripts.Pooling;
using System.Linq;

namespace Game.Scripts
{
    /// <summary>
    /// Singleton class to control wave
    /// </summary>
    public class WaveController 
    {
        public static WaveController Instance
        { 
            get
            { 
                if (_instance == null)
                {
                    _instance = new WaveController();
                }

                return _instance;
            } 
        }

        private bool _isStop = false;

        private static WaveController _instance;

        private float _spawnedTime = Time.time;

        private float _timeToSpawn = UnityEngine.Random.Range(GameDefines.ENEMY_SPAWN_TIME_INTERVAL_RANDOMLY.x, GameDefines.ENEMY_SPAWN_TIME_INTERVAL_RANDOMLY.y);

        private List<PoolObject> _pooledEnemies = new List<PoolObject>();

        private WaveController()
        {
        }

    	// Update is called once per frame
    	public void Update () 
        {
            if (_isStop)
                return;
            
            bool isSpawn = Time.time >= _spawnedTime + _timeToSpawn;

            if (isSpawn)
            {
                SpawnEnemy();

                // Prepare for next spawning
                _spawnedTime = Time.time;
                _timeToSpawn = UnityEngine.Random.Range(GameDefines.ENEMY_SPAWN_TIME_INTERVAL_RANDOMLY.x, GameDefines.ENEMY_SPAWN_TIME_INTERVAL_RANDOMLY.y);
            }
    	}

        public void EndWave()
        {
            _isStop = true;
            GameController.Instance.EnemyObjectPooling.ActiveObject.Where(x => x is EnemyController).Select(x => (EnemyController)x).ToList().ForEach(x =>
                {
                    x.IsStop = true;
                });
        }

        void SpawnEnemy()
        {
            var enemy = GameController.Instance.EnemyObjectPooling.Pop();
            enemy.gameObject.SetActive(true);
            enemy.transform.position = GameDefines.ENEMY_INITIAL_POSITION;
        }
    }
}
