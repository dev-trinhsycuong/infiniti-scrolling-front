﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Scripts
{
    public class GameDefines
    {
        public static string USER_NAME = "Cuong";

        public static float SCROLLING_BACKGROUND_SPEED = 0.25f;

        public static Vector3 ENEMY_INITIAL_POSITION = new Vector3(-17f,-3.55f,4.7f);

        public static Vector3 ENEMY_END_POSITION = new Vector3(18.5f,-3.55f,4.7f);

        public static float ENEMY_MOVE_SPEED = 10f;

        public static Vector2 ENEMY_SPAWN_TIME_INTERVAL_RANDOMLY = new Vector2(1, 3);

        public static int SCORE_FOR_EACH_PASSING = 10;
    }
}
