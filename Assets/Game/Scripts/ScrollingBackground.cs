﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Game.Scripts
{
    public class ScrollingBackground : MonoBehaviour 
    {
        public static ScrollingBackground Instance
        { 
            get
            { 
                return _instance;
            } 
        }

        private static ScrollingBackground _instance;

        public bool IsStop = false;

        [SerializeField] private MeshRenderer _renderer;

        private Vector2 _offset = Vector2.zero;

        void Awake()
        {
            _instance = this;
        }

    	// Use this for initialization
    	void Start () 
        {
            Observable.EveryUpdate().Subscribe(_ =>
                {
                    if(IsStop) return;

                    _offset.x += GameDefines.SCROLLING_BACKGROUND_SPEED * Time.deltaTime;

                    _renderer.material.mainTextureOffset = _offset;
                });
    	}
    }
}
